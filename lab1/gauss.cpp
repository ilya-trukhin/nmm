#include <iostream>


double * gauss(double **a, double *y, double *x, size_t n) {
	double max;
	size_t k, index;
	const double eps = 1e-12;  // точность
	
	k = 0;
	while (k < n) {
    		// Поиск строки с максимальным a[i][k]
		max = abs(a[k][k]);
		index = k;
		for (size_t i = k + 1; i < n; i++) {
			if (abs(a[i][k]) > max){
				max = abs(a[i][k]);
				index = i;
			}
		}
		// Перестановка строк
		if (max < eps) {
			// нет ненулевых диагональных элементов
			std::cout << "Решение получить невозможно из-за нулевого столбца ";
			std::cout << index << " матрицы A" << std::endl;
			return 0;
		}
		for (size_t j = 0; j < n; j++) {
			double temp = a[k][j];
			a[k][j] = a[index][j];
			a[index][j] = temp;
		}
		double temp = y[k];
		y[k] = y[index];
		y[index] = temp;
		// Нормализация уравнений
		for (size_t i = k; i < n; i++) {
			double temp = a[i][k];
			if (abs(temp) < eps) continue; // для нулевого коэффициента пропустить
			for (size_t j = 0; j < n; j++) 
				a[i][j] = a[i][j] / temp;
			y[i] = y[i] / temp;
			if (i == k)  continue; // уравнение не вычитать само из себя
			for (size_t j = 0; j < n; j++)
				a[i][j] = a[i][j] - a[k][j];
			y[i] = y[i] - y[k];
		}
		k++;
	}
	// обратная подстановка
	for (k = n - 1; k >= 0; k--){
		x[k] = y[k];
		for (size_t i = 0; i < k; i++)
			y[i] = y[i] - a[i][k] * x[k];
	}
	return x;
}