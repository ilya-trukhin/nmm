#ifndef _SPLINE_H_
#define _SPLINE_H_

#include <iostream>
#include <fstream>
#include <vector>

typedef struct point{
    double x, y;
} point;

extern double * gauss(double **a, double *y, double *x, int n);
extern double integral(double (*func)(double, double), double x0, double xn, double y0, double yn);

// количество точек x_i
extern size_t pointAmount;    
// точки x_i с заданными координатами x, y
extern std::vector<point> points;
// начальные и конечные координаты сетки
// netStartPoint - нижняя левая точка прямоугольной сетки,
// netEndPoint - верхняя правая
extern point netStartPoint, netEndPoint;
// число ячеек по осям OX и OY 
extern size_t netStepAmount_x, netStepAmount_y;
// координаты узлов сетки и величина шагов вдоль осей
extern std::vector<double> netNodesOX, netNodesOY;
extern double stepOX, stepOY;
// весовые коэффициенты
extern std::vector<double> weights;
// параметр регуляризации
extern double beta;

double getx_shift(double);
double gety_shift(double);
double phi(double x, double x0, double x1, size_t i);
double derilative2phi(double x, double x0, double x1, size_t i);
double local_psi(size_t i, double x, double y, size_t x_shift, size_t y_shift);
double global_psi(size_t i, double x, double y, size_t x_shift, size_t y_shift);
double derilative2_local_psi(size_t i, double x, double y, size_t x_shift, size_t y_shift);
double derilative2_global_psi(size_t i, double x, double y, size_t x_shift, size_t y_shift);
double integrand(double x, double y);

class spline{
private:
    // матрица левой части и вектор правой части
    double **a, *q, *b;
    // симметричная матрица для хранения интегралов
    double **ia;

    // методы
    void readData(const char*);
    void createNet();
    void createEquationComponents();
    void calcIntegral();
    void fillEquation();
    
public:
    spline(const char*);
    ~spline();
    void showNet();
    void showPoints();
    void createSpline();
};

#endif