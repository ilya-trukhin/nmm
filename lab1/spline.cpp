#include "spline.h"

// количество точек x_i
size_t pointAmount;    
// точки x_i с заданными координатами x, y
std::vector<point> points;
// начальные и конечные координаты сетки
// netStartPoint - нижняя левая точка прямоугольной сетки,
// netEndPoint - верхняя правая
point netStartPoint, netEndPoint;
// число ячеек по осям OX и OY 
size_t netStepAmount_x, netStepAmount_y;
// координаты узлов сетки и величина шагов вдоль осей
std::vector<double> netNodesOX, netNodesOY;
double stepOX, stepOY;
// весовые коэффициенты
std::vector<double> weights;
// параметр регуляризации
double beta;

// вспомогательные переменные
size_t curi, curj;

double phi(double x, double x0, double x1, size_t i){
	double ksi = (x - x0) / (x1 - x0);
	switch(i){
		case 1:{
			return 1 - 3*ksi*ksi + 2*ksi*ksi*ksi;
		}
		case 2:{
			return ksi - 2*ksi*ksi + ksi*ksi*ksi;
		}
		case 3:{
			return 3*ksi*ksi - 2*ksi*ksi*ksi;
		}
		case 4:{
			return -(ksi*ksi) + ksi*ksi*ksi;
		}
		default:
			return 0;
	}
}

// вторая производная по x
double derilative2phi(double x, double x0, double x1, size_t i){
    double ksi = (x - x0) / (x1 - x0);
	switch(i){
		case 1:{
			return (12*ksi - 6) / (x - x0)*(x - x0);
		}
		case 2:{
			return (6*ksi - 4) / (x - x0)*(x - x0);
		}
		case 3:{
			return (6 - 12*ksi) / (x - x0)*(x - x0);
		}
		case 4:{
			return (6*ksi - 2) / (x - x0)*(x - x0);
		}
		default:
			return 0;
	}
}

// i = 0 .. 15
double local_psi(size_t i, double x, double y, size_t x_shift, size_t y_shift){
    double x0 = netStartPoint.x + x_shift*stepOX;
    double y0 = netStartPoint.y + y_shift*stepOY;
	return phi(x, x0, x0 + stepOX, i % 4) * phi(y, y0, y0 + stepOY, i / 4);
}

// вычисление глобальных эрмитовых базисных функций на двумерной сетке
// p = points[i], x_shift = getx_shift(points[i].x), y_shift = gety_shift(points[i].y)
double global_psi(size_t i, double x, double y, size_t x_shift, size_t y_shift){
	return local_psi(i - 4*x_shift - 8*y_shift, x, y, x_shift, y_shift);
}

// i = 0 .. 15
double derilative2_local_psi(size_t i, double x, double y, size_t x_shift, size_t y_shift){
    double x0 = netStartPoint.x + x_shift*stepOX;
    double y0 = netStartPoint.y + y_shift*stepOY;
	return derilative2phi(x, x0, x0 + stepOX, i % 4) * phi(y, y0, y0 + stepOY, i / 4);
}

// вычисление глобальных эрмитовых базисных функций на двумерной сетке
// p = points[i], x_shift = getx_shift(points[i].x), y_shift = gety_shift(points[i].y)
double derilative2_global_psi(size_t i, double x, double y, size_t x_shift, size_t y_shift){
	return derilative2_local_psi(i - 4*x_shift - 8*y_shift, x, y, x_shift, y_shift);
}


void spline::readData(const char* pathToData){
    std::ifstream input(pathToData);
    if (input.is_open())
    {
        input >> pointAmount;
        
        for(size_t i =  0; i < pointAmount; i++){
            point p;
            input >> p.x >> p.y;
            points.push_back(p);
            weights.push_back(1); 
        }             
        input >> netStartPoint.x >> netStartPoint.y;
        input >> netEndPoint.x >> netEndPoint.y;
        input >> netStepAmount_x >> netStepAmount_y;
        input >> beta;
    }
    input.close();     // закрываем файл
}

void spline::createNet(){
    //netNodesOX = new double[netStepAmount_x + 1];
    //netNodesOY = new double[netStepAmount_y + 1];
    stepOX = (netEndPoint.x - netStartPoint.x) / netStepAmount_x; 
    stepOY = (netEndPoint.y - netStartPoint.y) / netStepAmount_y;
    
    netNodesOX.push_back(netStartPoint.x);
    for(size_t i = 1; i < netStepAmount_x; i++){
        netNodesOX.push_back(netNodesOX[i - 1] + stepOX);
    }
    netNodesOX.push_back(netEndPoint.x);

    netNodesOY.push_back(netStartPoint.y);
    for(size_t i = 1; i < netStepAmount_y; i++){
        netNodesOY.push_back(netNodesOY[i - 1] + stepOY);
    }
    netNodesOY.push_back(netEndPoint.y);
}

double getx_shift(double x){
    for(size_t i = 1; i < netStepAmount_x; i++)
        if(netNodesOX[i] > x)
            return i - 1;
    return netStepAmount_x - 1;
}
    
double gety_shift(double y){
    for(size_t i = 1; i < netStepAmount_y; i++)
        if(netNodesOY[i] > y)
            return i - 1;
    return netStepAmount_y - 1;
}

void spline::createEquationComponents(){
    // вычисление размера матрицы и вектора правой части
    size_t size = 4 * netStepAmount_x * netStepAmount_y;
    // выделение памяти для матриц и вектора
    a = new double*[size];
    ia = new double*[size];
    for(size_t i = 0; i < size; i++){
        a[i] = new double[size];
        ia[i] = new double[size - i];
    }
    
    b = new double[size];
    q = new double[size];
    //std::cout << "Memory has allocated" << std::endl;
}

double integrand(double x, double y){
    return derilative2_global_psi(curi, x, y, getx_shift(x), gety_shift(y)) * 
        derilative2_global_psi(curj, x, y, getx_shift(x), gety_shift(y));
}

void spline::calcIntegral(){
    size_t size = 4 * netStepAmount_x * netStepAmount_y;
    for(size_t i = 0; i < size; i++){
        curi = i;
        for(size_t j = 0; j < size - i; j++){
            curj = j + i;
            ia[i][j] += beta * integral(integrand, points[0].x, points[pointAmount - 1].x, points[0].y, points[pointAmount - 1].y);
        }
        std::cout << "a[" << i << "]" << std::endl;
    }
}

void spline::fillEquation(){
    
    size_t size = 4 * netStepAmount_x * netStepAmount_y;
    // диагональ и верхний треугольник
    for(size_t i = 0; i < size; i++){
        for(size_t j = i; j < size; j++){
            a[i][j] = 0;     
            for(size_t k = 0; k < pointAmount; k++){
                a[i][j] += weights[k] * 
                    global_psi(i, points[k].x, points[k].y, getx_shift(points[k].x), gety_shift(points[k].y)) * 
                    global_psi(j, points[k].x, points[k].y, getx_shift(points[k].x), gety_shift(points[k].y));
            }
            a[i][j] += ia[i][j - i];
        }
        //std::cout << "a[" << i << "]" << std::endl;
    }
    // нижний треугольник
    for(size_t i = 1; i < size; i++){
        for(size_t j = 0; j < i; j++){
            a[i][j] = 0;     
            for(size_t k = 0; k < pointAmount; k++){
                a[i][j] += weights[k] * 
                    global_psi(i, points[k].x, points[k].y, getx_shift(points[k].x), gety_shift(points[k].y)) * 
                    global_psi(j, points[k].x, points[k].y, getx_shift(points[k].x), gety_shift(points[k].y));
            }
            a[i][j] += ia[i - j][i];
        }
        //std::cout << "a[" << i << "]" << std::endl;
    }
}

// public
spline::spline(const char* pathToData){

    readData(pathToData);
    createNet();
}

void spline::showNet(){
    std::cout << "OX" << std::endl;
    for(size_t i = 0; i < netStepAmount_x + 1; i++){
        std::cout << netNodesOX[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "OY" << std::endl;
    for(size_t i = 0; i < netStepAmount_y + 1; i++){
        std::cout << netNodesOY[i] << " ";
    }
    std::cout << std::endl;
}

void spline::showPoints(){
    for(size_t i = 0; i < pointAmount; i++){
        double x0 =  getx_shift(points[i].x) * stepOX + netStartPoint.x;
        double y0 =  gety_shift(points[i].y) * stepOY + netStartPoint.y;

        std::cout << points[i].x << " from [ " <<  x0 << "; " << x0 + stepOX << " ]" << std::endl;
        std::cout << points[i].y << " from [ " << y0 << "; " << y0 + stepOY << " ]" << std::endl;
    }
}

spline::~spline(){
    size_t size = 4 * netStepAmount_x * netStepAmount_y;
    for(size_t i = 0; i < size; i++){
        delete[] a[i];
        delete[] ia[i];
    }
    
    delete[] a;
    delete[] ia;
    delete[] b;
    delete[] q;
}

void spline::createSpline(){
    createEquationComponents();
    calcIntegral();
    fillEquation();
}