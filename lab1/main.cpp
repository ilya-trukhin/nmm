#include "spline.h"


double func(double x){
	return x*x - 3*x + 2; 
}

int main(int argc, char *argv[]){
	spline *sp = new spline("initdata.txt");
	sp->showNet();
	sp->showPoints();
	sp->createSpline();
	delete sp;

	return 0;
}



