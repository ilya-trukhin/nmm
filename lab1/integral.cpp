#include <iostream>
#include <omp.h>

double q(size_t i, size_t j, size_t m, size_t n){
	if((i == 0 || i == m) && (j == 0 || j == n))
		return 0.25;
	else if( ((i == 0 || i == m) && (j > 0 && j < n)) || ((j == 0 || j == n) && (i > 0 && i < m)) )
		return 0.5;
	else
		return 1; 
}

// Вычисление интеграла методом трапеций
double trapezoidalRule(double (*func)(double), double x0, double xn, int subInterval = 1000){
	double I = func(x0) + func(xn);
	double stepSize = (xn - x0) / subInterval;
	double tmp;

	for(int i = 1; i < subInterval; i++){
		tmp = x0 + i*stepSize;
		I += 2*func(tmp);
	}
	I *= stepSize / 2;
	return I;
}

double trapezoidalRule(double (*func)(double, double), double x0, double xn, double y0, double yn, size_t m = 1000, size_t n = 1000){
	double I = 0;
	double stepSizeOX = (xn - x0) / m;
	double stepSizeOY = (yn - y0) / n;
	//double xi, yi;
#pragma omp parallel for reduction(+:I)
	for(size_t i = 0; i < m; i++){
		for(size_t j = 0; j < n; j++){
			double xi = x0 + i*stepSizeOX;
			double yi = y0 + j*stepSizeOY;
			I += q(i, j, m, n) * func(xi, yi);
		}
	}
	I *= stepSizeOX * stepSizeOY;
	return I;
}

double integral(double (*func)(double), double x0, double xn){
	return trapezoidalRule(func, x0, xn);
}

double integral(double (*func)(double, double), double x0, double xn, double y0, double yn){
	return trapezoidalRule(func, x0, xn, y0, yn);
}